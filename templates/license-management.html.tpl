<h1 class="title is-1">Licenses</h1>
<div class="columns is-multiline" id="licenses">
</div>

<h3 class="title is-3">License list</h3>
<div class="table-container">
    <table class="table is-fullwidth">
        <thead>
            <tr>
                <th>License</th>
                <th>Used by</th>
            </tr>
        </thead>
        <tbody id="dependencies">

        </tbody>
    </table>
</div>
<script>
    $.get("reports/gl-license-management-report.json", function(data) {
        var licenses = data['licenses'];
        var color = "";
        for (var i in licenses) {
            if (licenses[i]["name"] == "unknown")
                color = "danger"
            else
                color = "dark"
            var elem = $(`
            <div class="column is-one-quarter">
                <article class="message is-${color}">
                    <div class="message-header">
                        <p>${licenses[i]["name"]}</p>
                    </div>
                    <div class="message-body">
                        <h2 class="title is-2 has-text-${color} has-text-centered">${licenses[i]["count"]}</h2>
                    </div>
                </article>
            </div>`);
            elem.appendTo($('#licenses'))
        }

        var deps = data['dependencies'];
        for (var i in deps) {
            if (deps[i]["license"]["name"] == "unknown")
                color = "danger"
            else
                color = "dark"
            var elem = $(`
            <tr>
                <td>
                    <a class="tag is-medium is-${color} has-text-weight-bold is-uppercase" href="${deps[i]['license']['url']}">${deps[i]['license']['name']}</a>
                </td>
                <td>${deps[i]['dependency']['name']}</td>
            </tr>`);
            elem.appendTo($('#dependencies'));
        }
    });
</script>