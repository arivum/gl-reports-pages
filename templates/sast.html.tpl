<h1 class="title is-1">Static Application Security Tests</h1>
<div class="columns">
    <div class="column is-one-quarter">
        <article id="message-critical" class="message is-danger">
            <div class="message-header">
                <p>Critical</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-danger has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-high" class="message is-warning">
            <div class="message-header">
                <p>High</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-warning has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-medium" class="message is-dark">
            <div class="message-header">
                <p>Medium</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-dark has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-low" class="message is-light">
            <div class="message-header">
                <p>Low</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-grey-dark has-text-centered">0</h2>
                </div>
            </article>
        </div>
    </div>
    <h3 class="title is-3">Vulnerability list</h3>
    <div class="table-container">
    <table class="table is-fullwidth">
        <thead>
            <tr>
                <th>Severity</th>
                <th>Vulnerability</th>
                <th>Confidence</th>
            </tr>
        </thead>
        <tbody id="vulnerabilities">
        </tbody>
    </table>
    </div>
    <script>
        $.get( "reports/gl-sast-report.json", function( data ) {
            var vulnerabilities = data["vulnerabilities"];
            var severities = {
                critical: 0,
                high: 0,
                medium: 0,
                low: 0,
            };
            var colors = {
                critial: "danger",
                high: "warning",
                medium: "dark",
                low: "light",
            }
            for(var i in vulnerabilities) {
                severities[vulnerabilities[i]["severity"].toLowerCase()] += 1;
                var elem = $(`<tr>
                    <td><span class="tag is-${colors[vulnerabilities[i]["severity"].toLowerCase()]} has-text-weight-bold is-uppercase">${vulnerabilities[i]["severity"]}</span></td>
                    <td><p class="title is-6">${vulnerabilities[i]["message"]}</p><p class="is-size-7">
                        <a href="${$('.repo').attr('href')}/blob/master/${vulnerabilities[i]["location"]["file"]}#L${vulnerabilities[i]["location"]["start_line"]}">${vulnerabilities[i]["location"]["file"]}</a>
                    </p>
                    <p class="subtitle is-size-7"><code>${vulnerabilities[i]["cve"]}</code></p>
                </td>
                
                <td>${vulnerabilities[i]["confidence"]}</td>
            </tr>`)
            elem.appendTo($('#vulnerabilities'));
            for(var k in severities) {
                $('#message-'+k+' .message-body h2').text(severities[k]);
            }
        }
    });
    
</script>