package main

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

type page struct {
	URIPath          string
	TemplateFileName string
	ReportFileName   string
	MenuTitle        string
}

type menuItem struct {
	Target   string
	Children *[]menuItem
	Title    string
}

type content struct {
	MenuItems []menuItem
	Content   string
	BaseURI   string
	RepoURI   string
}

func loadConfFile(fileName string) ([]page, error) {
	var pages = make([]page, 0)
	log.Infof("Found: %s", fileName)
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		return []page{}, fmt.Errorf("Read file: %v ", err)
	}
	err = yaml.Unmarshal(yamlFile, pages)
	if err != nil {
		return []page{}, fmt.Errorf("Unmarshal: %v ", err)
	}
	for _, p := range pages {
		if p.MenuTitle == "" {
			return []page{}, errors.New("MenuTitle: mandatory")
		}
		if p.TemplateFileName == "" {
			return []page{}, errors.New("TemplateFileName: mandatory")
		}
		if p.URIPath == "" {
			return []page{}, errors.New("URIPath: mandatory")
		}
		if _, err := os.Stat(p.TemplateFileName); os.IsNotExist(err) {
			return []page{}, errors.New("Template: file not found")
		}
		if p.URIPath != "" {
			if _, err := os.Stat(p.ReportFileName); os.IsNotExist(err) {
				return []page{}, errors.New("Report: file not found")
			}
		}
	}
	return pages, nil
}

func findReadme() string {
	for _, readme := range []string{"README", "README.md", "readme.md", "Readme.md", "README.MD", "Readme.MD", "Readme"} {
		if _, err := os.Stat(readme); !os.IsNotExist(err) {
			log.Infof("Found: %s", readme)
			return readme
		}
	}
	return ""
}

func config() []page {
	var pages = make([]page, 0)

	pages = append(pages, page{
		URIPath:          "",
		TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/index.html.tpl",
		ReportFileName:   findReadme(),
		MenuTitle:        "Home",
	})
	if _, err := os.Stat(".gitlab-reports-pages.yaml"); !os.IsNotExist(err) {
		pages, err = loadConfFile(".gitlab-reports-pages.yaml")
		if err != nil {
			log.Fatal(err)
		}
	} else if _, err := os.Stat(".gitlab/reports-pages.yaml"); !os.IsNotExist(err) {
		pages, err = loadConfFile(".gitlab/reports-pages.yaml")
		if err != nil {
			log.Fatal(err)
		}
	} else {
		if _, err := os.Stat("gl-container-scanning-report.json"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "container-security",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/container_scanning.html.tpl",
				ReportFileName:   "gl-container-scanning-report.json",
				MenuTitle:        "Security/Container",
			})
		}
		if _, err := os.Stat("gl-sast-report.json"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "sast-security",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/sast.html.tpl",
				ReportFileName:   "gl-sast-report.json",
				MenuTitle:        "Security/SAST",
			})
		}
		if _, err := os.Stat("gl-license-management-report.json"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "license-management",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/license-management.html.tpl",
				ReportFileName:   "gl-license-management-report.json",
				MenuTitle:        "License Management",
			})
		}
		if _, err := os.Stat("gl-dast-report.json"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "dast-security",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/dast.html.tpl",
				ReportFileName:   "gl-dast-report.json",
				MenuTitle:        "Security/DAST",
			})
		}

		if _, err := os.Stat("go-dependencies.dot"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "go-dependencies",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/go-dependencies.html.tpl",
				ReportFileName:   "go-dependencies.dot",
				MenuTitle:        "Golang/Dependencies",
			})
		}

		if _, err := os.Stat("go-coverage.html"); !os.IsNotExist(err) {
			pages = append(pages, page{
				URIPath:          "go-coverage",
				TemplateFileName: os.Getenv("SOURCE_DIR") + "templates/go-coverage.html.tpl",
				ReportFileName:   "go-coverage.html",
				MenuTitle:        "Golang/Coverage",
			})
		}
	}
	return pages
}

func copy(src string, dst string) {
	data, err := ioutil.ReadFile(src)
	if err != nil {
		log.Warnf("File %s: not found", src)
		return
	}
	stat, _ := os.Stat(src)
	err = ioutil.WriteFile(dst, data, stat.Mode())
	if err != nil {
		log.Warnf("File %s: not found", dst)
		return
	}
}

func makeMenu(pages []page) []menuItem {
	menu := make([]menuItem, 0)
	for _, p := range pages {
		splitted := strings.Split(p.MenuTitle, "/")
		if len(splitted) >= 2 {
			index := -1
			for i, item := range menu {
				if item.Title == splitted[0] {
					index = i
				}
			}
			if index == -1 {
				menu = append(menu, menuItem{
					Target: "",
					Title:  splitted[0],
					Children: &[]menuItem{
						menuItem{
							Target:   p.URIPath,
							Title:    splitted[1],
							Children: nil,
						},
					},
				})
			} else {
				*menu[index].Children = append(*menu[index].Children, menuItem{
					Target:   p.URIPath,
					Title:    splitted[1],
					Children: nil,
				})
			}

		} else {
			menu = append(menu, menuItem{
				Target:   p.URIPath,
				Title:    p.MenuTitle,
				Children: nil,
			})
		}
	}
	return menu
}

func main() {
	pages := config()
	ctnt := content{}
	os.MkdirAll("public/reports", os.ModePerm)
	copy(os.Getenv("SOURCE_DIR")+"templates/icon.png", "public/icon.png")
	copy(os.Getenv("SOURCE_DIR")+"templates/aes256.js", "public/aes256.js")
	copy(os.Getenv("SOURCE_DIR")+"templates/bulma.min.css", "public/bulma.min.css")
	ctnt.MenuItems = makeMenu(pages)
	ctnt.BaseURI = os.Getenv("CI_PAGES_URL")
	ctnt.RepoURI = os.Getenv("CI_PROJECT_URL")

	for _, p := range pages {
		log.Infof("Copying report file: %v", p.ReportFileName)
		if os.Getenv("ENCRYPTED_RESOURCES_PASSWORD") == "" {
			copy(p.ReportFileName, fmt.Sprintf("public/reports/%s", p.ReportFileName))
		} else if p.ReportFileName != "" {
			encryptCMD := exec.Command("openssl", "enc", "-aes-256-cbc", "-md", "md5", "-a", "-A", "-pass", fmt.Sprintf("pass:%s", os.Getenv("ENCRYPTED_RESOURCES_PASSWORD")), "-in", p.ReportFileName, "-out", fmt.Sprintf("%s.sec", p.ReportFileName))
			err := encryptCMD.Run()
			if err != nil {
				log.Warnf("Openssl: %v", err)
			}
			data, err := ioutil.ReadFile(fmt.Sprintf("%s.sec", p.ReportFileName))
			if err != nil {
				log.Warnf("File %s: not found", fmt.Sprintf("%s.sec", p.ReportFileName))
			}
			data = append([]byte("ENCRYPTED"), data...)
			err = ioutil.WriteFile(fmt.Sprintf("public/reports/%s", p.ReportFileName), data, 0644)
			if err != nil {
				log.Warnf("File %s: not found", fmt.Sprintf("public/reports/%s", p.ReportFileName))
			}
			if err != nil {
				log.Warnf("Encrypting: %v", p.ReportFileName)
			}
		}

		tmpContent, _ := ioutil.ReadFile(p.TemplateFileName)
		ctnt.Content = string(tmpContent)
		tmpl := template.Must(template.ParseFiles(os.Getenv("SOURCE_DIR") + "templates/base.html"))
		os.MkdirAll(fmt.Sprintf("public/%s", p.URIPath), os.ModePerm)
		file, err := os.Create(fmt.Sprintf("public/%s/index.html", p.URIPath))
		if err != nil {
			log.Errorf("create file: %v", err)
		}
		log.Infof("Creating page: %v", p.URIPath)
		err = tmpl.Execute(file, ctnt)
		if err != nil {
			log.Errorf("Template: %v", err)
		}
		file.Close()
	}
}
