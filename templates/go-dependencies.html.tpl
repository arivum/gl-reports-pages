  <svg style="width:100%;" height="1000"></svg>
  <script src="https://d3js.org/d3.v4.min.js"></script>
  <script src="https://cdn.rawgit.com/gmamaladze/d3-dot-graph/cf08847e/build/d3-dot-graph.js"></script>
  <script>
    
    var line = d3.line()
    .curve(d3.curveCatmullRom.alpha(0.5));
    
    var svg = d3.select("svg"),
    width = document.documentElement.clientWidth,
    height = document.documentElement.clientHeight;
    
    // width = +svg.attr("width"),
    // height = +svg.attr("height");
    
    var color = d3.scaleOrdinal(d3.schemeCategory20b);
    
    var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().distance(100).id(function(d) { return d.id; }))
    .force("charge", d3.forceManyBody().strength(-100))
    .force("collide", d3.forceCollide(60))
    .force("center", d3.forceCenter(width / 4, height / 2));
    
    d3.dot("reports/go-dependencies.dot", function(graph) {
      //if (error) throw error;
      
      // build the arrow.
      svg.append("svg:defs").selectAll("marker")
      .data(["end"])      // Different link/path types can be defined here
      .enter().append("svg:marker")    // This section adds in the arrows
      .attr("id", String)
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 35)
      .attr("refY", -0.5)
      .attr("markerWidth", 6)
      .attr("markerHeight", 6)
      .attr("orient", "auto")
      .append("svg:path")
      .attr("d", "M0,-5L10,0L0,5");;
      
      // add the links and the arrows
      var path = svg.append("svg:g").attr("class", "links").selectAll("path")
      .data(graph.links)
      .enter().append("svg:path")
      //    .attr("class", function(d) { return "link " + d.type; })
      .attr("marker-end", "url(#end)");
      
      var node = svg.append("g")
      .attr("class", "nodes")
      .selectAll("circle")
      .data(graph.nodes)
      .enter().append("g")
      
      node
      .append("circle")
      .attr("r", 20)
      .attr("fill", function(d,i) { 
        if(d.id.startsWith("arivum.de"))
          if(i == 0)
            return "#7b4173";
          else
            return "#a55194";
        else
          return color(i%4);
        })
      .call(d3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended));
      
      // add the text
      node.append("text")
      .attr("x", 22)
      .attr("dy", ".35em")
      .text(function(d) { return d.id; });
      
      simulation
      .nodes(graph.nodes)
      .on("tick", ticked);
      
      simulation.force("link")
      .links(graph.links);
      
      let linkGen = d3.linkVertical().x(function(d) { return d.x; })
      .y(function(d) { return d.y; });;
      
      var linkRad = d3.linkRadial()
      .angle(function(d) { return d.x; })
      .radius(function(d) { return d.y; });
      
      function ticked() {
        path.attr("d", function(d) {
          var dx = d.target.x - d.source.x,
          dy = d.target.y - d.source.y,
          dr = Math.sqrt(dx * dx + dy * dy);
          return "M" +
          d.source.x + "," +
          d.source.y + "A" +
          dr + "," + dr + " 0 0,1 " +
          d.target.x + "," +
          d.target.y;
        });
        node
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")"; });
        }
      });
      
      function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }
      
      function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }
      
      function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }
    </script>
