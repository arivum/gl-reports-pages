<h1 class="title is-1">Container Security Tests</h1>
<div class="columns">
    <div class="column is-one-quarter">
        <article id="message-high" class="message is-danger">
            <div class="message-header">
                <p>High</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-danger has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-medium" class="message is-warning">
            <div class="message-header">
                <p>Medium</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-warning has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-low" class="message is-dark">
            <div class="message-header">
                <p>Low</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-dark has-text-centered">0</h2>
            </div>
        </article>
    </div>
    <div class="column is-one-quarter">
        <article id="message-negligible" class="message is-light">
            <div class="message-header">
                <p>Negligible</p>
            </div>
            <div class="message-body">
                <h2 class="title is-2 has-text-grey-dark has-text-centered">0</h2>
            </div>
        </article>
    </div>
</div>
<h3 class="title is-3">Vulnerability list</h3>
<div class="table-container">
    <table class="table is-fullwidth">
        <thead>
            <tr>
                <th>Severity</th>
                <th>Vulnerability</th>
                <th>Fixed By</th>
            </tr>
        </thead>
        <tbody id="vulnerabilities">
        </tbody>
    </table>
</div>
<script>
    $.get("reports/gl-container-scanning-report.json", function(data) {
        var vulnerabilities = data["vulnerabilities"];
        var severities = {
            high: 0,
            medium: 0,
            low: 0,
            negligible: 0,
        };
        var colors = {
            high: "danger",
            medium: "warning",
            low: "dark",
            negligible: "light",
        }
        for (var i in vulnerabilities) {
            severities[vulnerabilities[i]["severity"].toLowerCase()] += 1;
            var elem = $(`<tr>
                    <td><span class="tag is-${colors[vulnerabilities[i]["severity"].toLowerCase()]} has-text-weight-bold is-uppercase">${vulnerabilities[i]["severity"]}</span></td>
                    <td><p class="title is-6">${vulnerabilities[i]["featurename"]} (${vulnerabilities[i]["featureversion"]} - ${vulnerabilities[i]["namespace"]} - <a href="${vulnerabilities[i]["link"]}">${vulnerabilities[i]["vulnerability"]}</a>)</p>
                </td>
                
                <td>${vulnerabilities[i]["fixedby"]}</td>
            </tr>`)
            elem.appendTo($('#vulnerabilities'));
            for (var k in severities) {
                $('#message-' + k + ' .message-body h2').text(severities[k]);
            }
        }
    });
</script>