<div id="main" class="content">
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js"></script>
<script>
    function format() {
        $('table').each(function(i, e) {
            var table = $("<div class='table-container'></div>").append($(e).clone());
            $(e).replaceWith(table);
        });
        addClasses("table", "table is-fullwidth");
        addClasses("h1", "title is-1");
        addClasses("h2", "title is-2");
        addClasses("h3", "title is-3");
        addClasses("h4", "title is-4");
        addClasses("h5", "title is-5");
        addClasses("h6", "title is-6");
    }

    function addClasses(selector, classes) {
        $(selector).each(function(i, e) {
            $(e).addClass(classes);
        });
    }
    $.get("reports/README.md", function(data) {
        // replace images
        data = data.replace(/!\[.*\]\(.*\)/g, "")
        var converter = new showdown.Converter();
        converter.setOption('tables', true);
        html = converter.makeHtml(data);
        document.getElementById("main").innerHTML = html;
        format();
    });
</script>