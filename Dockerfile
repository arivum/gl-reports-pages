FROM golang:1.12.6-alpine3.10 AS builder

COPY . /src
WORKDIR /src
ENV GO111MODULE=on

RUN apk add --update git && \
    go get -v && \
    go build


FROM alpine:3.10

WORKDIR /src

RUN apk add --update openssl

COPY --from=builder /src/gl-reports-pages /usr/bin/gl-reports-pages
COPY templates /src/templates

CMD gl-reports-pages